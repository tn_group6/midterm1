/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.midterm1;

/**
 *
 * @author HP
 */
public class MamaShop {
    String taste;
    String meatType;
    String vegetables;
    
    MamaShop(String taste, String meatType, String vegetables) {
        this.taste = taste;
        this.meatType = meatType;
        this.vegetables = vegetables;
    }
    
    public String toString(){
      return this.taste + this.meatType + this.vegetables;
      
    }

    public String getTaste() {
        return taste;
    }

    public String getMeatType() {
        return meatType;
    }

    public String getVegetables() {
        return vegetables;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    public void setMeatType(String meatType) {
        this.meatType = meatType;
    }

    public void setVegetables(String vegetables) {
        this.vegetables = vegetables;
    }
    
    
}
