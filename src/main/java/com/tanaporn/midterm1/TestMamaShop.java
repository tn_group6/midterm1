/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.midterm1;

/**
 *
 * @author HP
 */
public class TestMamaShop {
    public static void main(String[] args){
        MamaShop customer1 = new MamaShop("Tomyumkung", "Pork", "Yes");
        MamaShop customer2 = new MamaShop("Tomyumkung", "Mix", "Yes");
        MamaShop customer3 = new MamaShop("Mhusub", "Shrimp", "No");
        MamaShop customer4 = new MamaShop("Tomyumkung", "Pork", "No");
        MamaShop customer5 = new MamaShop("Mhusub", "Mix", "Yes");
        
        customer1.toString();
        System.out.println("customer1" + " " + customer1.taste + " " + customer1.meatType + " " + customer1.vegetables);
        customer2.toString();
        System.out.println("customer2" + " " + customer2.taste + " " + customer2.meatType + " " + customer2.vegetables);
        customer3.toString();
        System.out.println("customer3" + " " + customer3.taste + " " + customer3.meatType + " " + customer3.vegetables);
        customer4.toString();
        System.out.println("customer4" + " " + customer4.taste + " " + customer4.meatType + " " + customer4.vegetables);
        customer5.toString();
        System.out.println("customer5" + " " + customer5.taste + " " + customer5.meatType + " " + customer5.vegetables);
        
        customer1.setTaste("Mhusub");
        System.out.println("customer1 set" + " " + customer1.taste + " " + customer1.meatType + " " + customer1.vegetables);
        customer3.setMeatType("Pork");
        System.out.println("customer3 set" + " " + customer3.taste + " " + customer3.meatType + " " + customer3.vegetables);
        customer5.setVegetables("No");
        System.out.println("customer5 set" + " " + customer5.taste + " " + customer5.meatType + " " + customer5.vegetables);
        
    }
}
